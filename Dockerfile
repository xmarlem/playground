FROM scratch

ENV GO111MODULE=on

ADD ./playground /usr/bin/playground

EXPOSE 8080

ENTRYPOINT ["playground"]